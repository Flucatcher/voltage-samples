package com.mycompany.newmodule;


import voltage.controllers.*;
import voltage.core.*;
import voltage.core.Jack.JackType;
import voltage.sources.*;
import voltage.utility.*;
import voltage.processors.*;
import voltage.effects.*;
import java.awt.*;

// Add your own imports here
import java.awt.geom.*;
import java.util.*;
import java.util.stream.*;



public class MyModule extends VoltageModule

{

public MyModule( long moduleID, VoltageObjects voltageObjects )
{
   super( moduleID, voltageObjects, "My Company", "My Module", ModuleType.ModuleType_Utility, 4.0 );


   theCanvasControl = new VoltageCanvas( "theCanvasControl", "Canvas", this, 268, 268 );
   AddComponent( theCanvasControl );
   theCanvasControl.SetPosition( 10, 10 );

   randimizeButton = new VoltageButton( "randimizeButton", "Randomize", this );
   AddComponent( randimizeButton );
   randimizeButton.SetPosition( 10, 280 );
   randimizeButton.SetSize( 37, 20 );
   randimizeButton.SetSkin( "Mini Rectangle Blue" );
   randimizeButton.ShowOverlay( true );
   randimizeButton.SetOverlayText( "RND" );
   randimizeButton.SetOverlayTextFont( "<Sans-Serif>", 10, true, false );
   randimizeButton.SetOverlayTextColor( new Color( 0, 0, 0 ) );
   randimizeButton.SetOverlayArea( 0, 0, 0, 0 );
   randimizeButton.SetOverlayTextJustification( VoltageButton.Justification.Centered );
   randimizeButton.SetAutoRepeat( false );

   SetSkin( "cecf1986a3fc4189bdf1680ee327ff1b" );
}

//-------------------------------------------------------------------------------
//  public void Initialize()

//  Initialize will get called shortly after your module's constructor runs. You can use it to
//  do any initialization that the auto-generated code doesn't handle.
//-------------------------------------------------------------------------------
@Override
public void Initialize()
{
   // add your own code here
   
   for (int index = 0; index < points.length; index += 1) {
      points[index] = new Point2D.Double(0 , 0);
   }
   RandomizePoints();
   
   
   canvasHelper = new CanvasHelper(theCanvasControl, resolution);

   StartGuiUpdateTimer();
}


//-------------------------------------------------------------------------------
//  public void Destroy()

//  Destroy will get called just before your module gets deleted. You can use it to perform any
//  cleanup that's not handled automatically by Java.
//-------------------------------------------------------------------------------
@Override
public void Destroy()
{
   super.Destroy();
   // add your own code here



}


//-------------------------------------------------------------------------------
//  public boolean Notify( VoltageComponent component, ModuleNotifications notification, double doubleValue, long longValue, int x, int y, Object object )

//  Notify will get called when various events occur - control values changing, timers firing, etc.
//-------------------------------------------------------------------------------
@Override
public boolean Notify( VoltageComponent component, ModuleNotifications notification, double doubleValue, long longValue, int x, int y, Object object )
{
   // add your own code here
   switch( notification )
   {
      case Knob_Changed:   // doubleValue is the new VoltageKnob value
      {
      }
      break;
   
      case Slider_Changed:   // doubleValue is the new slider value
      {
      }
      break;
   
      case Button_Changed:   // doubleValue is the new button/toggle button value
      {
         if (doubleValue == 0) {
            if (component == randimizeButton) {
               RandomizePoints();
            }
         }
      }
      break;
   
      case Switch_Changed:   // doubleValue is the new switch value
      {
      }
      break;
   
      case Jack_Connected:   // longValue is the new cable ID
      {
      }
      break;
   
      case Jack_Disconnected:   // All cables have been disconnected from this jack
      {
      }
      break;
   
      case GUI_Update_Timer:   // Called every 50ms (by default) if turned on
      {
         if (bDraw)
         {
            bDraw = false;
            theCanvasControl.Invalidate();
         }
      }
      break;
   
      // Less-common notifications:
   
      case Named_Timer:   // object contains a String with the name of the timer that has fired
      {
      }
      break;
   
      case Canvas_Painting:   // About to paint canvas.  object is a java.awt.Rectangle with painting boundaries
      {
         canvasHelper.Draw(points);
      }
      break;
   
      case Canvas_Painted:   // Canvas painting is complete
      {
         if (!bDraw)
         {
            bDraw = true;
         }      
      }
      break;
   
      case Canvas_MouseMove:   // The mouse has moved over a Canvas object
      {
         if (selectedPoint != null) {
            SetPoint(selectedPoint, canvasHelper.GetXValue(x), canvasHelper.GetYValue(y));
         }
      }
      break;
   
      case Canvas_LeftButtonDown:   // The user has left-clicked on a Canvas object
      {
         selectedPoint = GetPoint(canvasHelper.GetXValue(x), canvasHelper.GetYValue(y));
      }
      break;
   
      case Canvas_LeftButtonUp:   // The user has released left-click on a Canvas object
      {
         selectedPoint = null;
      }
      break;
   
      case Canvas_RightButtonDown:   // The user has right-clicked on a Canvas object
      {
      }
      break;
   
      case Canvas_RightButtonUp:   // The user has released left-click on a Canvas object
      {
      }
      break;
   
      case Canvas_MouseLeave:   // The mouse has moved off a Canvas object
      {
      }
      break;
   
      case Control_DragStart:   // A user has started dragging on a control that has been marked as dragable
      {
      }
      break;
   
      case Control_DragOn:   // This control has been dragged over during a drag operation. object contains the dragged object
      {
      }
      break;
   
      case Control_DragOff:   // This control has been dragged over during a drag operation. object contains the dragged object
      {
      }
      break;
   
      case Control_DragEnd:   // A user has ended their drag on a control that has been marked as dragable
      {
      }
      break;
   
      case Label_Changed:   // The text of an editable text control has changed
      {
      }
      break;
   
      case SoundPlayback_Start: // A sound has begun playback
      {
      }
      break;
   
      case SoundPlayback_End:    // A sound has ended playback
      {
      }
      break;
   
      case Scrollbar_Position:    // longValue is the new scrollbar position
      {
      }
      break;
   
      case PolyVoices_Changed:    // longValue is the new number of Poly Voices
      {
      }
      break;
   
      case File_Dropped:        // 'object' is a String containing the file path
      {
      }
      break;
   
      case Preset_Loading_Start:  // called when preset loading begins
      {
      }
      break;
   
      case Preset_Loading_Finish:  // called when preset loading finishes
      {
      }
      break;
   }



   return false;
}


//-------------------------------------------------------------------------------
//  public void ProcessSample()

//  ProcessSample is called once per sample. Usually it's where you read
//  from input jacks, process audio, and write it to your output jacks.
//  Since ProcesssSample gets called 48,000 times per second, offload CPU-intensive operations
//  to other threads when possible and avoid calling native functions.
//-------------------------------------------------------------------------------
@Override
public void ProcessSample()
{
   // add your own code here



}


//-------------------------------------------------------------------------------
//  public String GetTooltipText( VoltageComponent component )

//  Gets called when a tooltip is about to display for a control. Override it if
//  you want to change what the tooltip displays - if you want a knob to work in logarithmic fashion,
//  for instance, you can translate the knob's current value to a log-based string and display it here.
//-------------------------------------------------------------------------------
@Override
public String GetTooltipText( VoltageComponent component )
{
   // add your own code here



   return super.GetTooltipText( component );
}


//-------------------------------------------------------------------------------
//  public void EditComponentValue( VoltageComponent component, double newValue, String newText )

//  Gets called after a user clicks on a tooltip and types in a new value for a control. Override this if
//  you've changed the default tooltip display (translating a linear value to logarithmic, for instance)
//  in GetTooltipText().
//-------------------------------------------------------------------------------
@Override
public void EditComponentValue( VoltageComponent component, double newValue, String newText )
{
   // add your own code here



   super.EditComponentValue( component, newValue, newText );
}


//-------------------------------------------------------------------------------
//  public void OnUndoRedo( String undoType, double newValue, Object optionalObject )

//  If you've created custom undo events via calls to CreateUndoEvent, you'll need to
//  process them in this function when they get triggered by undo/redo actions.
//-------------------------------------------------------------------------------
@Override
public void OnUndoRedo( String undoType, double newValue, Object optionalObject )
{
   // add your own code here



}


//-------------------------------------------------------------------------------
//  public byte[] GetStateInformation()

//  Gets called when the module's state gets saved, typically when the user saves a preset with
//  this module in it. Voltage Modular will automatically save the states of knobs, sliders, etc.,
//  but if you have any custom state information you need to save, return it from this function.
//-------------------------------------------------------------------------------
@Override
public byte[] GetStateInformation()
{
   // add your own code here



   return null;
}


//-------------------------------------------------------------------------------
//  public void SetStateInformation(byte[] stateInfo)

//  Gets called when this module's state is getting restored, typically when a user opens a preset with
//  this module in it. The stateInfo parameter will contain whatever custom data you stored in GetStateInformation().
//-------------------------------------------------------------------------------
@Override
public void SetStateInformation(byte[] stateInfo)
{
   // add your own code here
   

}


// Auto-generated variables
private VoltageButton randimizeButton;
private VoltageCanvas theCanvasControl;


// Add your own variables and functions here
CanvasHelper canvasHelper;
boolean bDraw = false;

double resolution = 0.025;
Point2D.Double selectedPoint = null;

Point2D.Double[] points = new Point2D.Double[5];

public class PointComparator implements Comparator<Point2D.Double> {
 
    @Override
    public int compare(Point2D.Double point1, Point2D.Double point2) {
        return point1.x > point2.x ? 1 : ((point1.x == point2.x) ? 0 : -1);
    }
}

void RandomizePoints() {
   for (int index = 0; index < points.length; index += 1) {
      points[index].x = Math.random();
      points[index].y = Math.random();
   }
   
   points[0].x = 0;
   points[points.length - 1].x = 1;
   
   Arrays.sort(points, new PointComparator());
}

Point2D.Double GetPoint(double x, double y) {
   for (int index = 0; index < points.length; index += 1) {
      if (IsWithinPoint(points[index], x, y, resolution)) {
         return points[index];
      }
   }
   
   return null;
}

void SetPoint(Point2D.Double point, double x, double y) {
   if (point != null) {
      int index = IntStream.range(0, points.length)
               .filter(i -> point.equals(points[i]))
               .findFirst()
               .orElse(-1);
               
      if (index == 0) {
         x = 0;
      } else {
         x = Math.max(x, points[index - 1].x + resolution);
      }
      
      if (index == points.length - 1) {
         x = 1;
      } else {
         x = Math.min(x, points[index + 1].x - resolution);
      }
      
      point.y = y;
      point.x = x;
   }
}

public boolean IsWithinPoint(Point2D.Double point, double x, double y, double resolution) {
   return Math.abs(x - point.x) < resolution && Math.abs(y - point.y) < resolution;
}

class CanvasHelper {
   VoltageCanvas canvas;
   int canvasWidth, canvasHeight;
   double canvasDrawWidth, canvasDrawHeight;
   double circleSize;
   
   double canvasOffset = 4;
	Color backgroundColor = new Color(35, 31, 32);
	
	Color linearColor = new Color(90, 176, 237);
	Stroke linearStroke = new BasicStroke(3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
   
   
   public CanvasHelper(VoltageCanvas canvas, double resolution) {
      this.canvas = canvas;
   
      canvasWidth = canvas.GetWidth();
      canvasHeight = canvas.GetHeight();
      canvasDrawWidth = canvasWidth - 2 * canvasOffset;
      canvasDrawHeight = canvasHeight - 2 * canvasOffset;
      
      circleSize = canvasDrawWidth * resolution;
   }
   
   public void Draw(Point2D.Double[] points) {
      Graphics2D g = canvas.GetGraphics();
   
      g.setColor(backgroundColor);
      g.fillRect(0, 0, canvasWidth, canvasHeight);
      
      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

      g.setColor(linearColor);
      g.setStroke(linearStroke);
      
      Path2D.Double path = new Path2D.Double();
      for (int index = 0; index < points.length; index += 1) {
         if (index == 0) {
            path.moveTo(canvasOffset + canvasDrawWidth * points[index].x, canvasOffset + canvasDrawHeight * (1 - points[index].y));
         } else {
            path.lineTo(canvasOffset + canvasDrawWidth * points[index].x, canvasOffset + canvasDrawHeight * (1 - points[index].y));
         }
      }
      g.draw(path);
      
      for (int index = 0; index < points.length; index += 1) {
         Ellipse2D.Double circle = new Ellipse2D.Double(canvasOffset + canvasDrawWidth * points[index].x - circleSize / 2, canvasOffset + canvasDrawHeight * (1 - points[index].y) - circleSize / 2, circleSize, circleSize);
         g.fill(circle);
      }
   }   
   
	public double GetXValue(int x) {
	   return Math.min(1, Math.max(0, (x - canvasOffset) / canvasDrawWidth));
	}
	
	public double GetYValue(int y) {
	   return Math.min(1, Math.max(0, 1 - (y - canvasOffset) / canvasDrawHeight));
	}
}


}

 